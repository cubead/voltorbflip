package ModelPackage;

import ModelPackage.StrategyPackage.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Model{

    private Indicator secretBoardIndicator;
    private Boardset boardset;
    private List<Integer> data;

    public Model() throws IOException {
        Boardmaker boardmaker = new Boardmaker();

        long start = System.nanoTime();
            int wincount = 0;
        for(int i = 0; i < 1000; i++){

            Board secretBoard = boardmaker.generateBoard(0);

            secretBoardIndicator = new Indicator(secretBoard);

            //System.out.println(secretBoardIndicator.generateAllBoards().length);
            //System.out.println(secretBoard);
            Game game = new Game(secretBoard);

            if(secretBoardIndicator.generateAllBoards().length > 30){
                //continue;
            }

            BFSStrategy ps = new BFSStrategy();
            if(ps.playGame(game)){
                wincount++;
                //System.out.println("Game won");
            }
            else{
                //System.out.println("Game lost");
            }

            //System.out.println("ItCount: " + ps.treeCounter);

        }

        System.out.println(wincount);
        System.out.println((System.nanoTime() - start) / 1000000 );

    }
}