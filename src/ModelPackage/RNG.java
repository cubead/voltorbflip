package ModelPackage;

import java.util.Random;

public final class RNG{

    private static Random ran = new Random(0);

    //[min, max]
    public static int integer(int min, int max){
        return ran.nextInt(max - min + 1) + min;
    }

    public static void reset(){
        ran = new Random(0);
    }

}