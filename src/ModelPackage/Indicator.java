package ModelPackage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Indicator{
    //[0] horizontal sums
    private int[][] sumOfPoints = new int[2][5];
    private int[][] sumOfVoltOrbs = new int[2][5];

    public Indicator(Board board){
        for(int i = 0; i < 5; i++){
            for (int j = 0; j < 5; j++) {
                sumOfPoints[0][i] += board.getCard(j, i);
                sumOfPoints[1][i] += board.getCard(i, j);

                if(board.getCard(j, i) == 0) sumOfVoltOrbs[0][i]++;
                if(board.getCard(i, j) == 0) sumOfVoltOrbs[1][i]++;
            }
        }
    }

    public Board[] generateAllBoards(){
        List<Board> list = backtrackBoard(new Board(), new int[2][5], new int[2][5], 0, 0);
        Board[] array = new Board[list.size()];
        list.toArray(array);

        return array;
    }

    private List<Board> backtrackBoard(Board current, int[][] sum, int[][] zeros, int x, int y){
        List<Board> subset = new ArrayList<>();
        if(y == 5){
            subset.add(current.copy());
            return subset;
        }

        //Set a zero
        if(zeros[0][y] < sumOfVoltOrbs[0][y] && zeros[1][x] < sumOfVoltOrbs[1][x] &&
                !(x == 4 && (sum[0][y] != sumOfPoints[0][y] || zeros[0][y] + 1 != sumOfVoltOrbs[0][y])) &&
                !(y == 4 && (sum[1][x] != sumOfPoints[1][x] || zeros[1][x] + 1 != sumOfVoltOrbs[1][x]))) {

            zeros[0][y]++;
            zeros[1][x]++;

            current.setCard(x, y, (byte)0);
            subset.addAll(backtrackBoard(current, sum, zeros, (x + 1) % 5, y + x / 4));
            current.setCard(x, y, (byte)-1);

            zeros[0][y]--;
            zeros[1][x]--;
        }

        //Set 1/2/3
        for(byte i = 1; i <= 3; i++){
            if(sum[0][y] + i > sumOfPoints[0][y]) break;
            if(sum[1][x] + i > sumOfPoints[1][x]) break;

            if((x == 4 && (sum[0][y] + i != sumOfPoints[0][y] || zeros[0][y] != sumOfVoltOrbs[0][y])) ||
                    (y == 4 && (sum[1][x] + i != sumOfPoints[1][x] || zeros[1][x] != sumOfVoltOrbs[1][x]))){
                continue;
            }

            sum[0][y] += i;
            sum[1][x] += i;
            current.setCard(x, y, i);

            subset.addAll(backtrackBoard(current, sum, zeros, (x + 1) % 5, y + x / 4));

            current.setCard(x, y, (byte)-1);
            sum[0][y] -= i;
            sum[1][x] -= i;
        }

        return subset;
    }

    @Override
    public String toString() {
        String s = "      ";

        for(int i = 0; i < 5; i++){
            s += "(" + sumOfPoints[1][i] + " " + sumOfVoltOrbs[1][i] + ") ";
        }

        s += "\n";
        for(int i = 0; i < 5; i++){
            s += "(" + sumOfPoints[0][i] + " " + sumOfVoltOrbs[0][i] + ")\n";
        }

        return s;
    }
}