package ModelPackage;

import java.util.ArrayList;

public class Boardmaker{

    private int[][][] leveldesign;

    public Boardmaker(){
        // https://bulbapedia.bulbagarden.net/wiki/Voltorb_Flip
        this.leveldesign = new int[8][5][3];
        this.leveldesign[0][0] = new int[] {3, 1, 6};
        this.leveldesign[0][1] = new int[] {0, 3, 6};
        this.leveldesign[0][2] = new int[] {5, 0, 6};
        this.leveldesign[0][3] = new int[] {2, 2, 6};
        this.leveldesign[0][4] = new int[] {4, 1, 6};
        this.leveldesign[1][0] = new int[] {1, 3, 7};
        this.leveldesign[1][1] = new int[] {6, 0, 7};
        this.leveldesign[1][2] = new int[] {3, 2, 7};
        this.leveldesign[1][3] = new int[] {0, 4, 7};
        this.leveldesign[1][4] = new int[] {5, 1, 7};
        this.leveldesign[2][0] = new int[] {2, 3, 8};
        this.leveldesign[2][1] = new int[] {7, 0, 8};
        this.leveldesign[2][2] = new int[] {4, 2, 8};
        this.leveldesign[2][3] = new int[] {1, 4, 8};
        this.leveldesign[2][4] = new int[] {6, 1, 8};
        this.leveldesign[3][0] = new int[] {3, 3, 8};
        this.leveldesign[3][1] = new int[] {0, 5, 8};
        this.leveldesign[3][2] = new int[] {8, 0, 10};
        this.leveldesign[3][3] = new int[] {5, 2, 10};
        this.leveldesign[3][4] = new int[] {2, 4, 10};
        this.leveldesign[4][0] = new int[] {7, 1, 10};
        this.leveldesign[4][1] = new int[] {4, 3, 10};
        this.leveldesign[4][2] = new int[] {1, 5, 10};
        this.leveldesign[4][3] = new int[] {9, 0, 10};
        this.leveldesign[4][4] = new int[] {6, 2, 10};
        this.leveldesign[5][0] = new int[] {3, 4, 10};
        this.leveldesign[5][1] = new int[] {0, 6, 10};
        this.leveldesign[5][2] = new int[] {8, 1, 10};
        this.leveldesign[5][3] = new int[] {5, 3, 10};
        this.leveldesign[5][4] = new int[] {2, 5, 10};
        this.leveldesign[6][0] = new int[] {7, 2, 10};
        this.leveldesign[6][1] = new int[] {4, 4, 10};
        this.leveldesign[6][2] = new int[] {1, 6, 13};
        this.leveldesign[6][3] = new int[] {9, 1, 13};
        this.leveldesign[6][4] = new int[] {6, 3, 10};
        this.leveldesign[7][0] = new int[] {0, 7, 10};
        this.leveldesign[7][1] = new int[] {8, 2, 10};
        this.leveldesign[7][2] = new int[] {5, 4, 10};
        this.leveldesign[7][3] = new int[] {2, 6, 10};
        this.leveldesign[7][4] = new int[] {7, 3, 10};
    }

    public Board generateBoard(int level){
        int sublevel = RNG.integer(0,4);
        return generateBoard(level, sublevel);
    }

    public Board generateBoard(int level, int sublevel) {
        Board board = new Board();
        ArrayList<int[]> emptyCards = new ArrayList<>();
        for (int x = 0; x < 5; x++){
            for (int y = 0; y < 5; y++){
                int[] tupel = new int[] {x, y};
                emptyCards.add(tupel);
            }
        }
        byte insertNumber;
        for (int value = 0; value < 3; value++) {
            for (int i = 0; i < this.leveldesign[level][sublevel][value]; i++) {
                if (value == 0){
                    insertNumber = 2;
                }
                else if (value == 1){
                    insertNumber = 3;
                }
                else{
                    insertNumber = 0;
                }
                int randomCard = RNG.integer(0, emptyCards.size()-1);
                board.setCard(emptyCards.get(randomCard)[0], emptyCards.get(randomCard)[1], insertNumber);
                emptyCards.remove(randomCard);
            }
        }
        insertNumber = 1;
        for (int remainingCard = 0; remainingCard < emptyCards.size(); remainingCard++){
            board.setCard(emptyCards.get(remainingCard)[0], emptyCards.get(remainingCard)[1], insertNumber);
        }
        return board;
    }

}