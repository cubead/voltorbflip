package ModelPackage;

import java.util.ArrayList;
import java.util.List;

public class Boardset{

    Board[] possibleBoards;

    public Boardset(Board[] boards){
        possibleBoards = boards;
    }

    public Boardset searchBoardset(int x, int y, byte value){
        List<Board> boards = new ArrayList<>();
        for (Board board : possibleBoards) {
            if(board.getCard(x,y) == value){
                boards.add(board);
            }
        }
        Board[] ret = new Board[boards.size()];
        boards.toArray(ret);
        return new Boardset(ret);
    }

    public int getAmount(){
        return possibleBoards.length;
    }

    public  int[][][] getStats(){
        int[][][] ret = new int[5][5][4];
        for (int x = 0; x < 5; x++) {
            for (int y = 0; y < 5; y++) {
                for (Board possibleBoard : possibleBoards) {
                    ret[x][y][possibleBoard.getCard(x, y)]++;
                }
            }
        }
        return ret;
    }

    @Override
    public int hashCode() {
        int hash = possibleBoards.length;

        //for(int i = 0; i < possibleBoards.length; i++){
        //    hash ^= possibleBoards[i].hashCode();
        //}

        return  hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Boardset){
            Boardset cast = (Boardset)obj;

            if(possibleBoards.length != cast.possibleBoards.length)
                return false;

            for(int i = 0; i < possibleBoards.length; i++){
                 if(possibleBoards[i] != cast.possibleBoards[i])
                     return false;
            }
        }

        return  false;
    }
}