package ModelPackage.StrategyPackage;

public class CompactBoard {
    long data = 0;
    int revealed = 0;

    public  CompactBoard(){

    }
    private CompactBoard(long val, int rev ){
        data = val;
        revealed = rev;
    }

    public byte GetCard(int x, int y){
        if(((revealed >> (x + y * 5)) & 1) == 0) return -1;
        return (byte)((data >> (x + y * 5)) & 3);
    }
    public byte GetCard(int i){
        if(((revealed >> i) & 1) == 0) return -1;
        return (byte)((data >> i) & 3);
    }

    public void SetCard(int x, int y, int val){
        byte prev = GetCard(x,y);
        revealed |= 1 << (x + y * 5);
        data ^= (prev ^ val) << (x + y * 5);
    }

    public CompactBoard Copy(){
        return new CompactBoard(data, revealed);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompactBoard that = (CompactBoard) o;
        return data == that.data && revealed == that.revealed;
    }

    @Override
    public int hashCode() {
        return (int)(data ^ (data >> 32)) ^ (revealed << 16);
    }
}
