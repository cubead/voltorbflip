package ModelPackage.StrategyPackage;

import ModelPackage.*;

import java.util.*;

public class PerfectStrategy extends Strategy {
    HashMap<CompactBoard, DecisionTree> closeList = new HashMap<>(1000000);

    @Override
    public boolean playGame(Game game) {
        List<int[]> openList = new ArrayList<>(25);
        for (int x = 0; x < 5; x++) {
            for (int y = 0; y < 5; y++) {
                openList.add(new int[]{x,y});
            }
        }

        boolean[][] revealed = new boolean[5][5];
        DecisionTree tree = new DecisionTree(new Boardset(game.getIndicator().generateAllBoards()), new CompactBoard());
        Boardset bs = new Boardset(game.getIndicator().generateAllBoards());

        while(game.getGamestate() == -1){
            int x =  tree.DecisionX;
            int y = tree.DecisionY;

            //System.out.println(x + " " + y + " winProb: " + tree.WinProb);

            if(x == - 1 && y == - 1){
                int[][][] data = bs.getStats();
                for (int i = 0; i < 5; i++) {
                    for (int j = 0; j < 5; j++) {
                        if(data[i][j][2] + data[i][j][3] > 0 && !revealed[i][j]){
                            //System.out.println(i + " " + j);
                            try{
                                game.revealCard(i,j);
                            }
                            catch(Exception e){
                                return  false;
                            }
                        }
                    }
                }

                return  game.getGamestate() == 1;
            }

            byte val = -1;
            try{
                val = game.revealCard(x,y);
            }
            catch(Exception e){
                return  false;
            }

            if(val == 0) return  false;

            revealed[x][y] = true;
            bs = bs.searchBoardset(x,y, val);
            tree = tree.SubTrees[val - 1];
        }

        return game.getGamestate() == 1;
    }

    public long treeCounter = 0;
    class DecisionTree{
        double WinProb = 0;
        byte DecisionX = - 1, DecisionY = - 1;
        DecisionTree[] SubTrees = new DecisionTree[3];

        public DecisionTree(Boardset boardset, CompactBoard board){
            if(treeCounter++ % 100000 == 0){
                //System.out.println(treeCounter - 1);
            }
            int[][][] data = boardset.getStats();

            //winCheck
            boolean solved = true;
            for(int x = 0; x < 5; x ++){
                for(int y = 0; y < 5; y ++){
                    int[] buffer = data[x][y];
                    if(board.GetCard(x,y) == -1){
                        if(!(buffer[2] == boardset.getAmount() ||
                            buffer[3] == boardset.getAmount() ||
                            buffer[2] + buffer[3] == 0)){
                            solved = false;
                            break;
                        }
                    }
                    else{
                        for(int i = 0; i < 4; i++){
                            if(buffer[i] == boardset.getAmount()){
                                board.SetCard(x,y, i);
                                break;
                            }
                        }
                    }
                }
            }

            closeList.put(board, this);

            if(solved){
                WinProb = 1;
                return;
            }

            //Safe branches
            for(int i = 0; i < 25; i++){
                if(board.GetCard(i) == -1){
                    int x = i % 5;
                    int y = i / 5;
                    if(data[x][y][0] == 0){
                        WinProb = 0;
                        for(int j = 0; j < 3; j++){
                            //if(data[x][y][j + 1] == 0) continue;

                            CompactBoard buffer = board.Copy();
                            buffer.SetCard(x,y, j + 1);
                            Boardset bs = boardset.searchBoardset(x,y,(byte)(j + 1));

                            if(closeList.containsKey(buffer)){
                                SubTrees[j] = closeList.get(buffer);
                            }
                            else{
                                SubTrees[j] = new DecisionTree(bs, buffer);
                            }

                            WinProb += SubTrees[j].WinProb * bs.getAmount();
                        }

                        WinProb /= boardset.getAmount();

                        DecisionX = (byte)x;
                        DecisionY = (byte)y;

                        return;
                    }
                }
            }

            //unsafe branches
            DecisionTree[] treeBuffer = new DecisionTree[3];
            for(int i = 0; i < 25; i++){
                if(board.GetCard(i) == -1){
                    int x = i % 5;
                    int y = i / 5;

                    if(data[x][y][0] != boardset.getAmount() &&
                            data[x][y][2] + data[x][y][3] > 0){
                        double dProb = 0;
                        for(int j = 0; j < 3; j++){
                            Boardset bs = boardset.searchBoardset(x,y,(byte)(j + 1));
                            CompactBoard buffer = board.Copy();
                            buffer.SetCard(x,y, j + 1);

                            if(closeList.containsKey(buffer)){
                                treeBuffer[j] = closeList.get(buffer);
                            }
                            else{
                                treeBuffer[j] = new DecisionTree(bs, buffer);
                            }

                            dProb += treeBuffer[j].WinProb * bs.getAmount();
                        }
                        dProb /= boardset.getAmount();

                        if(dProb > WinProb){
                            WinProb = dProb;

                            for(int j = 0; j < 3; j++){
                                SubTrees[j] = treeBuffer[j];
                            }

                            DecisionX = (byte)x;
                            DecisionY = (byte)y;
                        }
                    }
                }
            }
        }
    }
}
