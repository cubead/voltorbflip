package ModelPackage.StrategyPackage;

import ModelPackage.Boardset;
import ModelPackage.Indicator;

import java.util.ArrayList;
import java.util.List;

public class HighRewardStrategy extends Strategy {

    public boolean playGame(ModelPackage.Game game){
        Indicator indicator = game.getIndicator();

        Boardset set = new Boardset(indicator.generateAllBoards());
        int[][][] data;

        List<int[]> openList = new ArrayList<>(25);
        for (int x = 0; x < 5; x++) {
            for (int y = 0; y < 5; y++) {
                openList.add(new int[]{x,y});
            }
        }

        while(game.getGamestate() == -1){
            data = set.getStats();

            int best = 0;
            int selected = -1;

            for (int i = 0; i < openList.size(); i++) {
                int x = openList.get(i)[0];
                int y = openList.get(i)[1];

                if(data[x][y][0] == 0){
                    selected = i;
                    break;
                }

                if(data[x][y][2] + data[x][y][3] > best){
                    best = data[x][y][2] + data[x][y][3];
                    selected = i;
                }
            }

            byte val = -1;
            try{
                val = game.revealCard(openList.get(selected)[0], openList.get(selected)[1]);
            }
            catch(Exception e){
                return  false;
            }

            //System.out.println(openList.get(selected)[0] + " | "  + openList.get(selected)[1] + " > " + val + " zeroChance: " + best * 100f / set.getAmount() + "% Ammount: " + set.getAmount() );
            set = set.searchBoardset(openList.get(selected)[0], openList.get(selected)[1], val);
            openList.remove(selected);
        }

        return game.getGamestate() == 1;
    }
}
