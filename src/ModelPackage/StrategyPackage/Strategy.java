package ModelPackage.StrategyPackage;

public abstract class Strategy {

    public abstract boolean playGame(ModelPackage.Game game) throws Exception;
}
