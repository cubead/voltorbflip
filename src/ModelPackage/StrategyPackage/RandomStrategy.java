package ModelPackage.StrategyPackage;

import ModelPackage.Boardset;
import ModelPackage.Indicator;
import ModelPackage.RNG;

import java.util.ArrayList;
import java.util.List;

public class RandomStrategy extends Strategy {

    public boolean playGame(ModelPackage.Game game) {
        List<int[]> openList = new ArrayList<>(25);
        for (int x = 0; x < 5; x++) {
            for (int y = 0; y < 5; y++) {
                openList.add(new int[]{x,y});
            }
        }

        Indicator indicator = game.getIndicator();

        Boardset set = new Boardset(indicator.generateAllBoards());
        int[][][] data;

        while(game.getGamestate() == -1){
            data = set.getStats();

            int selected = -1;

            for (int i = 0; i < openList.size(); i++) {
                int x = openList.get(i)[0];
                int y = openList.get(i)[1];

                if(data[x][y][0] == 0){
                    selected = i;
                }

                if(data[x][y][0] == set.getAmount()){
                    openList.remove(i--);
                }
            }

            if(selected == - 1) RNG.integer(0, openList.size()-1);

            byte val = -1;
            try{
                val = game.revealCard(openList.get(selected)[0], openList.get(selected)[1]);
            }
            catch(Exception e){
                return  false;
            }

            set = set.searchBoardset(openList.get(selected)[0], openList.get(selected)[1], val);
            openList.remove(selected);
        }

        return game.getGamestate() == 1;
    }
}
