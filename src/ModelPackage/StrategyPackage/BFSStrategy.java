package ModelPackage.StrategyPackage;

import ModelPackage.Board;
import ModelPackage.Boardset;
import ModelPackage.Game;
import javafx.util.Pair;

public class BFSStrategy extends Strategy{
    @Override
    public boolean playGame(Game game){
        Boardset bs = new Boardset(game.getIndicator().generateAllBoards());
        CompactBoard cb = new CompactBoard();

        while(game.getGamestate() == -1){
            DecisionTree dt = new DecisionTree(bs, cb.Copy(), 0);

            Pair<int[], Double> kek = dt.FindBestDecision();
            int x = kek.getKey()[0];
            int y = kek.getKey()[1];

            //System.out.println(x + " " + y + " zero chance: " + kek.getValue());
            byte val = -1;
            try{
                val = game.revealCard(x,y);
            }
            catch (Exception e){
                return false;
            }

            bs = bs.searchBoardset(x,y, val);
            cb.SetCard(x,y,val);
        }

        return game.getGamestate() == 1;
    }

    private class DecisionTree{
        final int MaxDepth = 4;

        int depth;
        Boardset boardset = null;
        DecisionTree[][][] subTrees = null;
        CompactBoard board;

        public DecisionTree(Boardset boards, CompactBoard board, int depth){
            this.depth = depth;
            this.board = board;
            boardset = boards;

            if(depth < MaxDepth){
                subTrees = new DecisionTree[5][5][];

                int[][][] data = boards.getStats();
                for(int x = 0; x < 5; x++){
                    for(int y = 0; y < 5; y++){
                        if(board.GetCard(x,y) != - 1) continue;
                        if(data[x][y][2] + data[x][y][3] == 0) continue;

                        subTrees[x][y] = new DecisionTree[3];
                        for(int i = 0; i < 3; i++){
                            if(data[x][y][i + 1] == 0) continue;
                            CompactBoard buffer = board.Copy();
                            buffer.SetCard(x,y, i + 1);
                            subTrees[x][y][i] = new DecisionTree(boardset.searchBoardset(x,y,(byte)(i + 1)), buffer,depth + 1);
                        }
                    }
                }
            }
        }

        public Pair<int[], Double> FindBestDecision(){
            if(depth == MaxDepth){
                return MakeDecision();
            }
            else{
                double best = Double.MAX_VALUE;
                int[] dec = null;

                for(int x = 0; x < 5; x++){
                    for(int y = 0; y < 5; y++){
                        if(best == 0) break;
                        if(board.GetCard(x,y) != -1) continue;
                        if(subTrees[x][y] == null) continue;

                        double sum = 0;
                        int counter = 0;
                        for(int i = 0; i < 3; i++){
                            if(subTrees[x][y][i] == null) continue;
                            Pair<int[], Double> move = subTrees[x][y][i].FindBestDecision();
                            counter += subTrees[x][y][i].boardset.getAmount();
                            sum += move.getValue() * subTrees[x][y][i].boardset.getAmount();
                        }

                        sum += 1.0 * (boardset.getAmount() - counter);
                        sum /= boardset.getAmount();

                        if(sum < best){
                            best = sum;
                            dec = new int[]{x,y};
                        }
                    }
                }

                return new Pair<>(dec, dec == null ? 0 : best);
            }
        }


        //low zero no one
        private Pair<int[], Double> MakeDecision(){
            int[][][] data = boardset.getStats();

            int selected = -1;
            int best = boardset.getAmount();
            for(int x = 0; x < 5; x++){
                for(int y = 0; y < 5; y++) {
                    if(board.GetCard(x,y) != -1) continue;

                    if(data[x][y][0] < best && data[x][y][2] + data[x][y][3] > 0){
                        best = data[x][y][0];
                        selected = x + y * 5;
                    }
                }
            }

            return new Pair<>(new int[]{selected % 5, selected / 5 }, best / (double)boardset.getAmount());
        }
    }
}
