package ModelPackage;

public class Game {

    private Board secretBoard;

    //-1 playing, 0 lost, 1 won
    private int gamestate;
    private int remainingRewards;
    private boolean[][] revealedCards = new boolean[5][5];

    public Game(Board board){
        secretBoard = board;

        gamestate = -1;
        for (int x = 0; x < 5; x++) {
            for (int y = 0; y < 5; y++) {
                if(secretBoard.getCard(x,y) > 1){
                    remainingRewards++;
                }
            }
        }
    }

    public Indicator getIndicator(){
        return  new Indicator(secretBoard);
    }

    public int getGamestate(){
        return gamestate;
    }

    public byte revealCard(int x, int y) throws Exception {
        if(gamestate != - 1) throw new Exception("The game has finished.");
        if(revealedCards[x][y]) throw new Exception("The card is already revealed.");

        byte val = secretBoard.getCard(x,y);

        if(val == 0) gamestate = 0;
        if(val > 1) remainingRewards--;

        if(remainingRewards == 0)
            gamestate = 1;

        revealedCards[x][y] = true;

        return val;
    }

}
