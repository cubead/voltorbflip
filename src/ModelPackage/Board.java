package ModelPackage;

public class Board{

    private byte[][] cards = new byte[5][5];

    public Board(){
        for (int x = 0; x < 5; x++){
            for (int y = 0; y < 5; y++){
                this.cards[x][y] = -1;
            }
        }
    }

    public Board(byte[][] cards){
        this.cards = cards;
    }

    public void setCard(int x, int y, byte value){
        cards[x][y] = value;
    }

    public byte getCard(int x, int y){
        return cards[x][y];
    }

    public Board copy(){
        byte[][] copyCards = new byte[5][5];
        for (int x = 0; x < 5; x++) {
            for (int y = 0; y < 5; y++) {
                copyCards[x][y] = cards[x][y];
            }
        }

        return new Board(copyCards);
    }

    @Override
    public String toString() {
        String s = "";

        for (int y = 0; y < 5; y++) {
            for (int x = 0; x < 5; x++) {
                s += cards[x][y];
                s += " ";
            }

            s += "\n";
        }

        return s;
    }
}