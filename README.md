# VoltorbFlip

Voltorb Flip is a minigame in Pokémon HeartGold and SoulSilver. This software helps the player by providing the best choice one can make from a probabilistic point of view during every situation of the minigame.